﻿function GetControlVariableName(control) {
    var assocVarName = control.getAttribute("data-forte-variable");
    return assocVarName === undefined ? "key" : assocVarName;
}

function GetControlValue(control) {
    var controlType = control.getAttribute("data-forte-control-type");
    var idBase = control.id.substring(3);

    switch (controlType) {
        case "MultilineCombo":
            return control.children["txtMain" + idBase].value;
        case "Combo":
            return control.children["cmbMain" + idBase].value;
        default:
            return control.value;
    }
}

function GetPrefill() {
    var forteCtls = document.getElementsByClassName("forte-control");
    var dataString = "<Prefill>";
    for (var ctl of forteCtls) {
        var value = GetControlValue(ctl);
        var name = GetControlVariableName(ctl);
        dataString += "<" + name + ">" + GetControlValue(ctl) + "</" + name + ">";
    }

    return dataString + "</Prefill>";
}

function ShowPrefill() {
    var prefill = GetPrefill();
    alert(prefill);
}

function MlcAddSelection(control) {
    var newText = "";
    var newline = String.fromCharCode(13, 10);
    var idBase = control.id.substring(3);
    var selVal = control.children["lstMain" + idBase].value;
    var mainTextbox = control.children["txtMain" + idBase];
    var existingText = "";
    existingText = mainTextbox.value;

    if (existingText !== "") {
        newText = existingText + newline + selVal;
    }
    else {
        newText = selVal;
    }

    mainTextbox.value = newText;
}

function MlcCloseDropdown(control) {
    var idBase = control.id.substring(3);
    var dd = control.children["lstMain" + idBase];
    dd.style.display = "none";
}

function MlcToggleDropdown(control) {
    var val = GetControlValue(control);
    var idBase = control.id.substring(3);
    var dd = control.children["lstMain" + idBase];
    var isVisible = dd.style.display !== "none";
    dd.style.display = isVisible ? "none" : "inline";
}
