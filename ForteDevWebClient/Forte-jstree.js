﻿var lastnode;
var editmode = false;

function OnLoadHandler() {
	$('#forte-segment-tree')
	    .jstree({
	                'core': {
	            'themes': {
	                'icons':true
	                }
	                },
	                'plugins' : ['wholerow']
	})
	$('#forte-segment-tree').on("close_node.jstree", PreventVariableCloseNode);
	$('#forte-segment-tree').on("open_node.jstree", OnVariableNodeOpen);
	$('#forte-segment-tree').on("select_node.jstree", OnVariableNodeSelect);
	$('#forte-segment-tree').on('click', ':input', EnterEditMode);
	
	var v =$("#forte-segment-tree").jstree(true).get_json('#', {'flat': true});
	for (i = 0; i < v.length; i++) {
	    var z = v[i];
	    VariableNodeInitialize(z.id);
	}
}
function PreventVariableCloseNode(evt, data) {
	if (data.node.li_attr.class.includes('forte-variable'))
	{
		$('#forte-segment-tree').jstree('open_node', data.node.id);
	}
}
function OnVariableNodeSelect(evt, data) {
	var node = [];
	if (data.node.li_attr.class.includes("forte-variable-display-value"))
	{
		$('#forte-segment-tree').jstree('select_node', data.node.parent);
		return;
	}
	else if (data.node.li_attr.class.includes("forte-variable-wrapper"))
	{
		node = data.node;
	}
	else
	{
		return;
	}
	if (node != lastnode)
	{
		var ID1 = node.children[0];
		var ID2 = node.children[1];
		$('#forte-segment-tree').jstree('hide_node', ID1);
		$('#forte-segment-tree').jstree('show_node', ID2);
		if (lastnode != undefined) {
			$('#forte-segment-tree').jstree('hide_node', lastnode.children[1]);
			$('#forte-segment-tree').jstree('show_node', lastnode.children[0]);
		}
		lastnode = node;
	}
}
function OnVariableNodeOpen(evt,data){
	if (data.node.li_attr.class.includes("forte-variable-wrapper"))
	{
		if (data.node != lastnode)
		{
			var ID1 = data.node.children[0];
			var ID2 = data.node.children[1];
			$('#forte-segment-tree').jstree('hide_node', ID1);
			$('#forte-segment-tree').jstree('show_node', ID2);
			if (lastnode != undefined) {
				$('#forte-segment-tree').jstree('hide_node', lastnode.children[1]);
				$('#forte-segment-tree').jstree('show_node', lastnode.children[0]);
			}
			lastnode = data.node;
		}
	}
}
function VariableNodeInitialize(node_id) {
	var node = $("#forte-segment-tree").jstree().get_node(node_id);
	if (node.li_attr.class.includes("forte-variable-wrapper"))
	{
		var ID1 = node.children[0];
		var ID2 = node.children[1];
		$('#forte-segment-tree').jstree('hide_node', ID2);
		$('#forte-segment-tree').jstree('show_node', ID1);
	}
}
function EnterEditMode (evt) {
		evt.stopPropagation();

};