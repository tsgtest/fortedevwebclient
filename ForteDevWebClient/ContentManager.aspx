﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>Word Add-In with Commands Sample</title>

    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/FabricUI/MessageBanner.js" type="text/javascript"></script>
    <script src="https://appsforoffice.microsoft.com/lib/1/hosted/office.js" type="text/javascript"></script>

    <!-- To enable offline debugging using a local reference to Office.js, use:                        -->
    <!-- <script src="Scripts/Office/MicrosoftAjax.js" type="text/javascript"></script>  -->
    <!-- <script src="Scripts/Office/1/office.js" type="text/javascript"></script>  -->

    <link href="ContentManager.css" rel="stylesheet" type="text/css" />
    <script src="ContentManager.js" type="text/javascript"></script>

    <!-- For the Office UI Fabric, go to https://aka.ms/office-ui-fabric to learn more. -->
    <link rel="stylesheet" href="https://appsforoffice.microsoft.com/fabric/2.1.0/fabric.min.css">
    <link rel="stylesheet" href="https://appsforoffice.microsoft.com/fabric/2.1.0/fabric.components.min.css">
    
    <!-- To enable the offline use of Office UI Fabric, use: -->
    <!-- link rel="stylesheet" href="Content/fabric.min.css" -->
    <!-- link rel="stylesheet" href="Content/fabric.components.min.css" -->

    <link href="jsTree\themes\default\style.min.css" rel="stylesheet">
    <script src="jstree/jstree.min.js"></script>
</head>
<body onload="OnLoadHandler()">
    <div id="content-main">
        <div class="padding">
            <!-- <input id="prefill" type="text" value="<IncludeTitle>true</IncludeTitle><Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index='1' UNID='0'>Daniel Fisherman</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='1' UNID='0'>Developer</TITLE><COMPANY Index='1' UNID='0'>TSG</COMPANY><COREADDRESS Index='1' UNID='0'>my address&#13;&#10;my city, my state my zip</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index='2' UNID='0'>Valerie Melville</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='2' UNID='0'>Manager</TITLE><COMPANY Index='2' UNID='0'>TSG</COMPANY><COREADDRESS Index='2' UNID='0'>her address&#13;&#10;her city, her state her zip</COREADDRESS><CIDetailTokenString>[CIDetail__FullNameWithPrefixAndSuffix][CIDetail__TitleIfBusiness][CIDetail__CompanyIfBusiness][CIDetail__COREADDRESS]</CIDetailTokenString></Recipients><UseTableFormat>false</UseTableFormat><Reline><Standard Format='0' Default='Alternate Insurance'>this is the reline</Standard><Special Format='0' Default='Alternate Insurance'><Label Index='1'>Claimant</Label><Value Index='1'>a</Value><Label Index='2'>Date of Claim</Label><Value Index='2'>b</Value><Label Index='3'>Company Name</Label><Value Index='3'>c</Value><Label Index='4'>Action Number</Label><Value Index='4'>d</Value></Special></Reline><Subject>This is the subject line</Subject><Agreed>true</Agreed><DateFormat>MMMM dd, yyyy</DateFormat><ExecuteonBlock>true</ExecuteonBlock><TitleProperty>doc title</TitleProperty><CustomTitle>custom doc title</CustomTitle><DocumentVariableValue>dv custom 1 var text</DocumentVariableValue><SetLanguage>true</SetLanguage><FontName>Calibri</FontName><FontSize>12.0</FontSize><BodyTextFirstLineIndent>1.0</BodyTextFirstLineIndent><BodyTextAlignment>0</BodyTextAlignment><AuthorName>Daniel C. Fisherman</AuthorName><Culture>1033</Culture><Authors>9358.0¦-1¦Daniel C. Fisherman¦7659872|</Authors>" /><br /> -->
            <div id="tree"></div>
            
            <button id="show-dlg">Show Dialog</button>
            <!--<button id="create-doc">Create Document</button><button id="nav-win">Show Dialog</button>-->
            <br />
            <label id="status-Header"></label>
            <label id="status-Body"></label>
        </div>
    </div>
    <div class="footer">
        <div class="ms-Grid ms-bgColor-themeSecondary">
            <div class="ms-Grid-row">
                <div class="ms-Grid-col ms-u-sm12 ms-u-md12 ms-u-lg12"> <div class="ms-font-xl ms-fontColor-white">The Sackett Group</div></div>
            </div>
        </div>
    </div>

    <!-- FabricUI component used for displaying notifications -->
    <div class="ms-MessageBanner" style="position:absolute;bottom: 0;">
        <div class="ms-MessageBanner-content">
            <div class="ms-MessageBanner-text">
                <div class="ms-MessageBanner-clipper">
                    <div class="ms-font-m-plus ms-fontWeight-semibold" id="notificationHeader"></div>
                    <div class="ms-font-m ms-fontWeight-semilight" id="notificationBody"></div>
                </div>
            </div>
            <button class="ms-MessageBanner-expand" style="display:none"><i class="ms-Icon ms-Icon--chevronsDown"></i> </button>
            <div class="ms-MessageBanner-action"></div>
        </div>
        <button class="ms-MessageBanner-close"> <i class="ms-Icon ms-Icon--x"></i> </button>
    </div>
</body>
</html>
