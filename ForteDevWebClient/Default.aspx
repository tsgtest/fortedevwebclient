﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>
            Forte Test Web Client</h1>
        <p>&nbsp;<asp:Button class="btn btn-primary btn-lg" ID="Button1" runat="server" Text="Show Letter Dialog" OnClick="Button1_Click1"  />
            <asp:DropDownList ID="SegmentID" runat="server">
                <asp:ListItem Value="3039">Letter</asp:ListItem>
                <asp:ListItem Value="461">Memo</asp:ListItem>
                <asp:ListItem Value="1476">Fax</asp:ListItem>
                <asp:ListItem Value="3607">CA Superior Pleading</asp:ListItem>
            </asp:DropDownList>
        </p>
    </div>

</asp:Content>
