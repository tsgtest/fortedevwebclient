﻿var lastvar;

//called on body load
function OnLoadHandler() {
    //subscribe to tree events
    $('.forte-segment-display-name').click(ToggleSegmentNodeExpansion);
    $('.forte-variable-display-name').click(ToggleVariableNodeExpansion);
    $('.forte-variable-display-name').each(CollapseVariableNodes);
    $('.forte-more-display-value').click(ToggleMoreNodeExpansion);
    $('.forte-more-display-value').each(ToggleMoreNodeExpansion);
}

//returns the variable name associated with the specified control
function GetControlVariableName(control) {
    var assocVarName = control.getAttribute("data-forte-variable");
    return assocVarName === undefined ? "key" : assocVarName;
}

//returns the value of the specified control
function GetControlValue(control) {
    var controlType = control.getAttribute("data-forte-control-type");
    var idBase = control.id.substring(3);

    switch (controlType) {
        case "MultilineCombo":
            return control.children["txtMain" + idBase].value;
        case "Combo":
            return control.children["cmbMain" + idBase].value;
        default:
            return control.value;
    }
}

//returns a Forte prefill string constructed from the current control values
function GetPrefill() {
    var forteCtls = document.getElementsByClassName("forte-control");
    var dataString = "<Prefill>";
    for (var ctl of forteCtls) {
        var value = GetControlValue(ctl);
        var name = GetControlVariableName(ctl);
        dataString += "<" + name + ">" + GetControlValue(ctl) + "</" + name + ">";
    }

    return dataString + "</Prefill>";
}

//displays the prefill in a message box
function ShowPrefill() {
    var prefill = GetPrefill();
    alert(prefill);
}

//adds selected items to the textbox of the specified multiline combo
function MlcAddSelection(control) {
    var newText = "";
    var newline = String.fromCharCode(13, 10);
    var idBase = control.id.substring(3);
    var selVal = control.children["lstMain" + idBase].value;
    var mainTextbox = control.children["txtMain" + idBase];
    var existingText = "";
    existingText = mainTextbox.value;

    if (existingText !== "") {
        newText = existingText + newline + selVal;
    }
    else {
        newText = selVal;
    }

    mainTextbox.value = newText;
}

//closes the dropdown of the specified multiline combo
function MlcCloseDropdown(control) {
    var idBase = control.id.substring(3);
    var dd = control.children["lstMain" + idBase];
    dd.style.display = "none";
}

//toggles the dropdown of the specified multiline combo
function MlcToggleDropdown(control) {
    var val = GetControlValue(control);
    var idBase = control.id.substring(3);
    var dd = control.children["lstMain" + idBase];
    var isVisible = dd.style.display !== "none";
    dd.style.display = isVisible ? "none" : "inline";
}

//expands/collapses the active segment node
function ToggleSegmentNodeExpansion() {
    $(this).parent().children().toggle();
    $(this).toggle();
}

function ToggleMoreNodeExpansion() {
    $(this).parent().children().toggle();
    $(this).toggle();
}
//expands/collapses the active variable node
function ToggleVariableNodeExpansion() {

    if (lastvar !== undefined) {
        lastvar.parent().find('li').hide();
        lastvar.parent().find('li').first().show();
    }
    $(this).parent().find('li').show();
    $(this).parent().find('li').first().hide();
    lastvar = $(this)
}

//expands only the active variable node
function CollapseVariableNodes() {
    $(this).parent().find('li').hide();
    $(this).parent().find('li').first().show();
}

// Reads data from current document selection and displays a notification
function CreateDocument() {
    Word.run(function (context) {
        var id = $('#tree').jstree(true).get_selected(true)[0].id;
        var segID = id.substring(id.indexOf(".") + 1, id.length);
        var dataToPassToService = {
            Type: "CreateDocument",
            Args: segID + "~" + $('#prefill').val()
        };

        $.ajax({
            url: 'api/ForteServer',
            type: 'POST',
            data: JSON.stringify(dataToPassToService),
            contentType: 'application/json;charset=utf-8'
        }).done(function (data) {
            //context.document.body.insertOoxml(data, "end")
            //does not insert headers/footers by design - see http://tickanswer.com/solved/2389978642/header-not-showing-up-when-im-inserting-a-document
            context.document.body.insertFileFromBase64(data, "End");
            context.sync();
            showNotification("Request complete.", "");
        }).fail(function (status) {
            showNotification("Request failed.", status);
        }).always(function () {
            // placeholder
        });
    });
}

// Reads data from current document selection and displays a notification
function GetTreeFolders(node, callback) {
    $.ajax({
        url: 'api/ForteServer',
        type: 'POST',
        data: { Type: "GetFolderMembersJSON", Args: "Daniel Fisherman~" + node.id },
        dataType: 'json'
    }).success(function (ret) {
        var json = eval("(" + ret + ")");
        callback.call(this, json);
    }).fail(function (status) {
        showNotification("Request failed.", status);
    }).always(function () {
        // placeholder
    });
}

//$$(Helper function for treating errors, $loc_script_taskpane_home_js_comment34$)$$
function errorHandler(error) {
    // $$(Always be sure to catch any accumulated errors that bubble up from the Word.run execution., $loc_script_taskpane_home_js_comment35$)$$
    showNotification("Error:", error);
    console.log("Error: " + error);
    if (error instanceof OfficeExtension.Error) {
        console.log("Debug info: " + JSON.stringify(error.debugInfo));
    }
}

// Helper function for displaying notifications
function showNotification(header, content) {
    $("#status-header").text(header);
    $("#status-body").text(content);
    //messageBanner.showBanner();
    //messageBanner.toggleExpansion();
}
