﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Text;
using System.Xml;

namespace ForteDevWebClient.TSG.ForteServer
{
    public class ForteServerController : ApiController
    {
        public class InstructionParams
        {
            public string Type { get; set; }
            public string Args { get; set; }
        }

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        //// POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
        [HttpPost()]
        public object ProcessInstruction(InstructionParams oParams)
        {
            string[] aArgs = oParams.Args.Split('~');

            switch (oParams.Type.ToLower())
            {
                case "createdocument":
                    return CreateDocument(aArgs[0], aArgs[1]);
                case "getfoldersxml":
                    return GetFoldersXml(aArgs[0], aArgs[1]);
                case "getfoldermembersjson":
                    string folders = GetFolderMembersJSON(aArgs[0], aArgs[1]);
                    folders = folders.Replace("\"text\" :", "\"parent\" : \"" + aArgs[1] + "\", \"text\" :");
                    return folders;
                case "getuihtml":
                    return GetUIHtml(aArgs[0], aArgs[1], aArgs[2]);
                default:
                    return null;
            }
        }

        public string GetUIHtml(string user, string xID, string xPrefill)
        {
            try
            {
                if(!string.IsNullOrEmpty(xPrefill))
                    xPrefill = ConvertXmlToDelimitedString("<Prefill>" + xPrefill + "</Prefill>");

                string addr = GetRequestingDeviceAddress("GetUIHtml");

                string instruction = "<ForteInstructions><ForteInstruction type=\"GetUIHtml\"><User>" + user + "</User><ID>" + xID + "</ID><Prefill>" + xPrefill +
                    "</Prefill><Format>" + "2" + "</Format><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";

                //add client functionality to Word, which will allow users to select segment and prefill, then send to server
                TSG.ForteServer.ForteServerClient oServer = new TSG.ForteServer.ForteServerClient();
                string xText = oServer.ProcessInstructionSet(instruction)[0];

                return xText;
            }
            catch (System.Exception oE)
            {
                return "Error: " + oE.Message + '\v' + oE.StackTrace;
            }
        }
        public string CreateDocument(string xID, string xPrefill)
        {
            try
            {
                //add client functionality to Word, which will allow users to select segment and prefill, then send to server
                TSG.ForteServer.ForteServerClient oServer = new TSG.ForteServer.ForteServerClient();

                //string xID1 = "6707";
                //string xID2 = "6709";
                //string xPrefill1 = "<Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index=\"1\" UNID=\"0\">Doug Miller</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index=\"1\" UNID=\"0\">Developer</TITLE><COMPANY Index=\"1\" UNID=\"0\">TSG</COMPANY><COREADDRESS Index=\"1\" UNID=\"0\">28 6th Ave. &#13;&#10;NY, NY 10003</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index=\"2\" UNID=\"0\">Linda Sackett</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index=\"2\" UNID=\"0\">Operations Manager</TITLE><COMPANY Index=\"2\" UNID=\"0\">TSG</COMPANY><COREADDRESS Index=\"2\" UNID=\"0\">Her address &#13;&#10;Her city, state, and zip</COREADDRESS><CIDetailTokenString>[CIDetail__FullNameWithPrefixAndSuffix][CIDetail__TitleIfBusiness][CIDetail__CompanyIfBusiness][CIDetail__COREADDRESS]</CIDetailTokenString><NewVariable1>900 Fifth Avenue, Suite 2500&#13;&#10;New York, New York  10003</NewVariable1></Recipients><NewVariable2>this is the variable 2 value</NewVariable2><Reline><Standard Format=\"0\" Default=\"Alternate Insurance\">This is the reline value</Standard><Special Format=\"0\" Default=\"Alternate Insurance\"><Label Index=\"1\">Claimant</Label><Value Index=\"1\">c</Value><Label Index=\"2\">Date of Claim</Label><Value Index=\"2\">d</Value><Label Index=\"3\">Company Name</Label><Value Index=\"3\">c</Value><Label Index=\"4\">Action Number</Label><Value Index=\"4\">a</Value></Special></Reline><AuthorInitials>DCF</AuthorInitials><ReLine2><Standard Format=\"0\" Default=\"Alternate Insurance\">reline 2  value akls;dfj ads ;ruwpqeo ruqoiewur o qeorpoie q r pqoiewur qw erpoiquweqe pqiweu pqo ewiuq pirqpeiuqper qepur qpweqeo r periuq ep qwpriu wrq er poe rqerpqiew rq ewroipqwueroipqi roiru</Standard></ReLine2><Culture>1033</Culture><Authors>9358.0¦-1¦Daniel C. Fisherman¦7659872|</Authors>";
                //string xPrefill2 = "<IncludeTitle>true</IncludeTitle><Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index='1' UNID='0'>Daniel Fisherman</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='1' UNID='0'>Developer</TITLE><COMPANY Index='1' UNID='0'>TSG</COMPANY><COREADDRESS Index='1' UNID='0'>my address&#13;&#10;my city, my state my zip</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index='2' UNID='0'>Valerie Melville</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='2' UNID='0'>Manager</TITLE><COMPANY Index='2' UNID='0'>TSG</COMPANY><COREADDRESS Index='2' UNID='0'>her address&#13;&#10;her city, her state her zip</COREADDRESS><CIDetailTokenString>[CIDetail__FullNameWithPrefixAndSuffix][CIDetail__TitleIfBusiness][CIDetail__CompanyIfBusiness][CIDetail__COREADDRESS]</CIDetailTokenString></Recipients><UseTableFormat>false</UseTableFormat><Reline><Standard Format='0' Default='Alternate Insurance'>this is the reline</Standard><Special Format='0' Default='Alternate Insurance'><Label Index='1'>Claimant</Label><Value Index='1'>a</Value><Label Index='2'>Date of Claim</Label><Value Index='2'>b</Value><Label Index='3'>Company Name</Label><Value Index='3'>c</Value><Label Index='4'>Action Number</Label><Value Index='4'>d</Value></Special></Reline><Subject>This is the subject line</Subject><Agreed>true</Agreed><DateFormat>MMMM dd, yyyy</DateFormat><ExecuteonBlock>true</ExecuteonBlock><TitleProperty>doc title</TitleProperty><CustomTitle>custom doc title</CustomTitle><DocumentVariableValue>dv custom 1 var text</DocumentVariableValue><SetLanguage>true</SetLanguage><FontName>Calibri</FontName><FontSize>12.0</FontSize><BodyTextFirstLineIndent>1.0</BodyTextFirstLineIndent><BodyTextAlignment>0</BodyTextAlignment><AuthorName>Daniel C. Fisherman</AuthorName><Culture>1033</Culture><Authors>9358.0¦-1¦Daniel C. Fisherman¦7659872|</Authors>";

                xPrefill = ConvertXmlToDelimitedString("<Prefill>" + xPrefill + "</Prefill>");
                string addr = GetRequestingDeviceAddress("CreateDocument");

                string instruction = "<ForteInstructions><ForteInstruction type=\"CreateDocument\"><ID>" + xID + "</ID><User>Daniel Fisherman</User><Prefill>" + xPrefill +
                    "</Prefill><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
                string xText = oServer.ProcessInstructionSet(instruction)[0];

                return xText;
            }
            catch (System.Exception oE)
            {
                return "Error: " + oE.Message + '\v' + oE.StackTrace;
            }
        }

        private string GetFoldersXml(string user, string parentID)
        {
            try
            {
                string addr = GetRequestingDeviceAddress("GetFoldersXml");
                string instruction = "<ForteInstructions><ForteInstruction type=\"GetFoldersXml\"><User>" + user + "</User><ParentFolderID>" + parentID +
                    "</ParentFolderID><EmailAddress>" + addr + "</EmailAddress></ForteInstruction></ForteInstructions>";

                TSG.ForteServer.ForteServerClient oServer = new TSG.ForteServer.ForteServerClient();
                string xml = oServer.ProcessInstructionSet(instruction)[0];

                return xml;
            }
            catch (System.Exception oE)
            {
                return "Error: " + oE.Message + '\v' + oE.StackTrace;
            }
        }

        private string GetFolderMembersJSON(string user, string parentID)
        {
            try
            {
                if (parentID == "#")
                {
                    return "[{id:'0.0', text: 'My Folders', parent: '#','state': {'selected': true}, 'children': true, 'icon': '/images/Folder.png'}, {id:'0', text: 'Public Folders', parent: '#', 'children': true, 'icon': '/images/Folder.png'},{id:'-2', text: 'Shared  Folders', parent: '#','children': true, 'icon': '/images/Folder.png'}]";
                }
                else
                {
                    string addr = GetRequestingDeviceAddress("GetFoldersXml");
                    string instruction = "<ForteInstructions><ForteInstruction type=\"GetFolderMembersJSON\"><User>" + user + "</User><ParentFolderID>" + parentID +
                        "</ParentFolderID><EmailAddress>" + addr + "</EmailAddress></ForteInstruction></ForteInstructions>";

                    TSG.ForteServer.ForteServerClient oServer = new TSG.ForteServer.ForteServerClient();
                    string json = oServer.ProcessInstructionSet(instruction)[0];
                    json = json.Replace("\"intendedUse\" : \"0\"", "\"children\" : true, \"icon\" : \"/images/Folder.png\"");
                    json = json.Replace("\"intendedUse\" : \"1\"", "\"icon\" : \"/images/Document.png\"");
                    json = json.Replace("\"intendedUse\" : \"2\"", "\"icon\" : \"/images/DocumentComponent.png\"");
                    json = json.Replace("\"intendedUse\" : \"3\"", "\"icon\" : \"/images/TextBlock.png\"");
                    json = json.Replace("\"intendedUse\" : \"4\"", "\"icon\" : \"/images/StyleSheet.png\"");
                    return json;
                }
            }
            catch (System.Exception oE)
            {
                return "Error: " + oE.Message + '\v' + oE.StackTrace;
            }
        }

        private string GetRequestingDeviceAddress(string cmd)
        {
            string strHostName = Dns.GetHostName();
            Console.WriteLine("Local Machine's Host Name: " + strHostName);
            // Then using host name, get the IP address list..
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string addr = "momshead@gmail.com";

            foreach (var ip in ipEntry.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork && ip.ToString().StartsWith("10."))
                {
                    addr = strHostName + " (IP: " + ip.ToString() + "; " + cmd + ")";
                    break;
                }
            }

            return addr;
        }
        private string ConvertXmlToDelimitedString(string xPrefill)
        {
            StringBuilder oSB = new StringBuilder();

            XmlDocument oXml = new XmlDocument();
            oXml.LoadXml(xPrefill);

            XmlNodeList oElements = oXml.DocumentElement.ChildNodes;
            foreach (XmlNode element in oElements)
            {
                oSB.AppendFormat("{0}¤{1}Þ", element.Name, element.InnerXml);
            }

            oSB.Remove(oSB.Length - 1, 1);
            return oSB.ToString();
        }
    }
}